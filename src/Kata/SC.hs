module Kata.SC (add) where

import Data.List
import Data.List.Split
import Text.Regex


add :: String -> Either [Int] Int
add input =
  let
    numbers =
      map read $ parse input

    negatives =
      filter (< 0) numbers
  in
    if null negatives then
      Right $ sum $ filter (< 1001) numbers
    else
      Left $ negatives


parse :: String -> [String]
parse input
  | isPrefixOf "//[" input =
    let
      inputs :: [String]
      inputs =
        splitOn "\n" input

      separator :: Regex
      separator =
        mkRegex $
          intercalate "|" $
          map escapeRe $
          sortBy (\a b -> if length a > length b then LT else GT) $
          splitOn "][" $
          init $
          drop 3 $
          inputs !! 0

      content :: String
      content =
        inputs !! 1
    in
      splitRegex separator content
  | "//" `isPrefixOf` input =
      splitRegex (mkRegex $ escapeRe $ take 1 $ drop 2 input) $ drop 4 input
  | otherwise =
      splitRegex (mkRegex ",|\n") input


escapeRe :: String -> String
escapeRe [] = []
escapeRe (x:xs)
    | x `elem` "*+.\\?|$^)({}" =
        '\\' : x : escapeRe xs
    | otherwise =
        x : escapeRe xs
